// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"
#include "DatosMemCompartida.h"
#include "Disparo.h"
#include "Socket.h"

class CMundo  
{
public:
	
	char ip[10]="127.0.0.1";
	Socket com;//socket para envio de datos
	char fichproy[100] ="/tmp/ProyeccionMemoria";
	int fdp;// descriptor de de proyeccion
	char nombres[2][50];
	char cliente[50];
	
	void Init();
	CMundo();
	~CMundo();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	
	void duplicarEsferas();
	void inicializarDisp(ListaDisparos list,int);
	
	std::vector<Esfera> bola; // almacen de bolas en el juego
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
	ListaDisparos ListDisp1;
	ListaDisparos ListDisp2;
	DatosMemCompartida MemCompartida;
	DatosMemCompartida* pMemCompartida;
	
	int num=0;
	int puntos1=0;
	int puntos2=0;
	int p1_anterior=0;
	int p2_anterior=0;
	int disp1=0;//conteo de disparos creados
	int disp2=0;//conteo de disparos creados
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
