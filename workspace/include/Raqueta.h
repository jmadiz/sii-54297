// Raqueta.h: interface for the Raqueta class.
//
//////////////////////////////////////////////////////////////////////
#pragma once 

#include "Plano.h"
#include "Vector2D.h"
#include "Esfera.h"
#include <vector>

class Raqueta : public Plano  
{
public:
	float centro;
	int disp; //numero de disparos creados
	Vector2D velocidad;
	
	Raqueta();
	virtual ~Raqueta();
	
	float Centro();
	void Mueve(float t);
	void reducirTam();
};
