// Esfera.h: interface for the Esfera class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ESFERA_H__8D520BAF_8208_423B_BD91_29F6687FB9C3__INCLUDED_)
#define AFX_ESFERA_H__8D520BAF_8208_423B_BD91_29F6687FB9C3__INCLUDED_


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Vector2D.h"

class Esfera  
{
public:	
	Esfera();
	~Esfera();
		
	Vector2D centro;
	Vector2D velocidad;
	float radio;
	int C2,C1;//colores

	virtual void Mueve(float t);
	void Dibuja();
	void setRadio(float);
	void setPos(float x, float y);
	void setVel(float x, float y);
	void setColor(int x, int y);
	void setCentro(Vector2D c);
	Vector2D getCentro();
	void reducirTam();
};

#endif // !defined(AFX_ESFERA_H__8D520BAF_8208_423B_BD91_29F6687FB9C3__INCLUDED_)
