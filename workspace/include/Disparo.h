// Raqueta.h: interface for the Raqueta class.
//
//////////////////////////////////////////////////////////////////////
#pragma once 

#include <vector>
#include "Esfera.h"
#include "Vector2D.h"

class ListaDisparos: public Esfera{
	
	int num=0;
	public:
	
	std::vector <Esfera> disp;
	void Dispara(int i,float);
	void DibujaDisp();
	void EliminarDisp(int);
	void Limpiar();
	int getNum(){return num;}
};
