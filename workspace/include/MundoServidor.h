// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include <string>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Esfera.h"
#include "Raqueta.h"
#include "DatosMemCompartida.h"
#include "Disparo.h"
#include "Socket.h"

class CMundo  
{
public:
	
	pthread_t thidJ1; // incluimos el atributo del hilo en el servidor
	pthread_attr_t atribJ1;
	pthread_t thidJ2; // incluimos el atributo del hilo en el servidor
	pthread_attr_t atribJ2;
	pthread_t thid2; // incluimos el atributo del hilo en el servidor
	pthread_attr_t atrib2;
	int fd; //Descriptor para tuberia logger
	char fich[100]= "Tuberia_logger"; //cadena que almacena el nombre de la tuberia. 
	
	void Init();
	CMundo();
	~CMundo();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	
	void duplicarEsferas();
	void RecibeComandosJugadorJ1();
	void RecibeComandosJugadorJ2();
	void GestionaConexiones();
	
	Socket servidor;
   	std::vector<Socket> conexiones;//vector de conexiones
   	char nombres[4][50];//lista de nombres
	std::vector<Esfera> bola; // almacen de bolas en el juego
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
	ListaDisparos ListDisp1;
	ListaDisparos ListDisp2;

	int acabar=0;//variable de desconexion de hilos
	int num=0;//numero de clientes conectados
	int puntos1=0;
	int puntos2=0;
	int p1_anterior=0;
	int p2_anterior=0;
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
