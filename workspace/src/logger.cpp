 #include <sys/types.h>
 #include <sys/stat.h>
 #include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>

int main (int argc, char* argv[]){
	
	char buf[100];
	int fd;
	int n_read;
	
	if(mkfifo("Tuberia_logger", 0777)<0){
		perror("Error en creacion de tuberia");
		return 1;
		}
	
	fd = open("Tuberia_logger", O_RDONLY);
	
	if(fd<0){
		perror("Error en lectura de tuberia");
		return 1;
		}
	
	while((n_read=read(fd,buf, 100 ))>0){
		
		if(write(1, buf, n_read)<n_read){
			perror("error en salida std");
			close(fd);
			unlink("Tuberia_logger");
			exit(1);
		}
		
	}
		
	unlink("Tuberia_logger");	
	return 0;
}

