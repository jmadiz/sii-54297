// Esfera.cpp: implementation of the Esfera class.
//
// AUTOR Juan Manuel Astorga Diz
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio= 0.5f;
	velocidad.x=10.0f;
	velocidad.y=10.0f;
	C1=255;
	C2=255;
}

Esfera::~Esfera()
{

}


///////////aca podemos establecer el color que queremos tener para la esfera
void Esfera::setColor(int x, int y){
	
	C1=x;
	C2=y;
}
////////// establecemos radio de la esfera
void Esfera::setRadio(float r){
	radio=r;
}

/////////// establecemos posicion

void Esfera::setPos(float x, float y){
	
	centro.x = x;
	centro.y = y;

}

////// establecemos velocidad
void Esfera::setVel(float x, float y){
	velocidad.x = x;
	velocidad.y = y;
}

void Esfera::reducirTam(){
	 
	 radio=radio - radio*0.1; 

}

void Esfera::Dibuja()
{
	glColor3ub(C1,C2,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
	centro.x= centro.x + velocidad.x*t;
	centro.y= centro.y + velocidad.y*t;	
}

Vector2D Esfera::getCentro(){
	return centro;
}

void Esfera::setCentro(Vector2D c){
	centro=c;
}
