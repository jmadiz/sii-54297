// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"
#include <iostream>
#include <string>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
//servicios
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <pthread.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

void* hilo_comandosJ1(void* d)//Hilo para recibir comandos del Jugador 1
{
      CMundo* p=(CMundo*) d;
      p->RecibeComandosJugadorJ1();
}

void* hilo_comandosJ2(void* d)////Hilo para recibir comandos del Jugador 2
{
      CMundo* p=(CMundo*) d;
      p->RecibeComandosJugadorJ2();
}

void* hilo_comandos2(void* d)//Hilo para recibir conexiones de clientes
{
      CMundo* p=(CMundo*) d;
      p->GestionaConexiones();
}


CMundo::CMundo()
{	
	Init();
	}

CMundo::~CMundo()
{
	acabar=1;
	pthread_join(thidJ1, NULL);
	pthread_join(thidJ2, NULL);
	pthread_join(thid2, NULL);
	bola.clear();
	close(fd);
	servidor.Close();
	for(int i=0;i<conexiones.size();i++){
		conexiones[i].Close();
	}
	conexiones.clear();
	printf("cerrar servidor\n");
}

void CMundo::GestionaConexiones(){//funcion que se encarga de agregar nuevo cliente
	while(!acabar){
		usleep(10);
		char nombre[50];
		Socket com_s;
		com_s = servidor.Accept();
		if(com_s.Receive(nombre,sizeof(nombre))>0){
				if(conexiones.size()<4){
				printf("Se ha conectado:%s\n",nombre);
				strcpy(nombres[num],nombre);
				conexiones.push_back(com_s);
				num++;
				}
				else printf("No se ha aceptado el ingreso a:%s\n",nombre);
			}
		}
	return;
}

void CMundo::RecibeComandosJugadorJ1()///funcion que elige los movimientos recibidos de la fifo con Cliente
{
     while (!acabar) {
     	if(conexiones.size()>1){
            usleep(10);
            char cad[100];
            unsigned char key;
            conexiones[0].Receive(cad, sizeof(cad)) ;
            sscanf(cad,"%c",&key);
            if(key=='s')jugador1.velocidad.y=-4;
            if(key=='w')jugador1.velocidad.y=4;
            if(key=='a')ListDisp1.Dispara(1,jugador1.Centro());
            }
      	}
      return;
}

void CMundo::RecibeComandosJugadorJ2()///funcion que elige los movimientos recibidos de la fifo con Cliente
{
     while (!acabar) {
     	if(conexiones.size()>1){
            usleep(10);
            char cad[100];
            unsigned char key;
            conexiones[1].Receive(cad, sizeof(cad)) ;
            sscanf(cad,"%c",&key);
            if(key=='l')jugador2.velocidad.y=-4;
            if(key=='o')jugador2.velocidad.y=4;
            if(key=='k')ListDisp2.Dispara(2,jugador2.Centro());      
            }
      	}
      return;
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	//////////////////////////////////inicio
	
	switch(conexiones.size()){
	case 0: {
		char cad[100];
		sprintf(cad,"Esperando 2 jugadores");
		print(cad,200,150,1,1,1);
		}break;
	
	case 1:{
		char cad[100];
		sprintf(cad,"Esperando 1 jugador");
		print(cad,200,150,1,1,1);
		}break;
	case 2:{
		char cad[100];
		sprintf(cad,"aceptando expectadores");
		print(cad,200,0,1,1,1);
		}break;
	case 3:{
		char cad[100];
		sprintf(cad,"Expectador:%s",nombres[2]);
		print(cad,200,0,1,1,1);
		}break;
		
	case 4:{
		char cad[200];
		sprintf(cad,"Expectadores:%s, %s",nombres[2],nombres[3]);
		print(cad,150,0,1,1,1);
		}break;
		}
	///////Puntos
	char cad[100];
	sprintf(cad,"%s: %d",nombres[0],puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"%s: %d",nombres[1],puntos2);
	print(cad,450,0,1,1,1);
	int i;
	
	if (puntos1==4 || puntos2==4){
		char cad[100];
		sprintf(cad,"FIN DEL JUEGO");
		print(cad,220,200,1,1,1);
	}
	
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	ListDisp1.DibujaDisp();
	ListDisp2.DibujaDisp();
	for (int i=0; i< bola.size();i++){
		bola[i].Dibuja();
	}

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)

{	
	
	if(num>1){//debe haber 2 jugadores minimo para que se hagan calculos
	////////////////////////////////
	// comunicacion con logger
	
	if ((puntos1>p1_anterior)||(puntos2>p2_anterior)){
		dprintf(fd, "%s con %i puntos, %s con %i puntos \n",nombres[0], puntos1,nombres[1], puntos2);
		//actualizacion del puntaje anterior para hacer comparacion de cambio
		p1_anterior=puntos1;
		p2_anterior=puntos2;
	}
	
	if (puntos1 == 4 ||puntos2 == 4) {
		if (puntos1>puntos2) dprintf(fd, "%s ha ganado, Fin de partida\n",nombres[0]);
		else dprintf(fd, "%s ha ganado, Fin de partida\n",nombres[1]);
		close(fd);
		bola.clear();
		ListDisp1.Limpiar();
		ListDisp2.Limpiar();
		}
	
	///////////////////////////////
	//movimiento de objetos
	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	///muevo la lista de esferas
	for (int i=0; i< bola.size();i++){
		bola[i].Mueve(0.025f);
	}
	
	///incluimos los movimientos de los disparos creados
	for (int i=0; i<ListDisp2.disp.size(); i++){
		ListDisp2.disp[i].Mueve(0.025f);
	}
	for (int i=0; i<ListDisp1.disp.size(); i++){

		ListDisp1.disp[i].Mueve(0.025f);
	}
	//////////rebote de la bola
	
	for (int j=0; j<bola.size(); j++){
		for(int i=0;i<paredes.size();i++)
		{ 
			paredes[i].Rebota(bola[j]);
			paredes[i].Rebota(jugador1);
			paredes[i].Rebota(jugador2);
		}
		jugador1.Rebota(bola[j]);
		jugador2.Rebota(bola[j]);
	}
	
	//condiciones eliminacion disparos
	
	for (int i=0; i<ListDisp1.disp.size(); i++){
		if (fondo_dcho.Rebota(ListDisp1.disp[i])||fondo_izq.Rebota(ListDisp1.disp[i])){
			ListDisp1.EliminarDisp(i);
			}
		if (jugador2.Rebota(ListDisp1.disp[i])){
			jugador2.reducirTam();
			ListDisp1.EliminarDisp(i);
		}
		if (jugador1.Rebota(ListDisp1.disp[i])){
			jugador1.reducirTam();
			ListDisp1.EliminarDisp(i);
		}
	}
	
	for (int i=0; i<ListDisp2.disp.size(); i++){
		if (fondo_dcho.Rebota(ListDisp2.disp[i])||fondo_izq.Rebota(ListDisp2.disp[i])){
			ListDisp2.EliminarDisp(i);
			}
		if (jugador2.Rebota(ListDisp2.disp[i])){
			jugador2.reducirTam();
			ListDisp2.EliminarDisp(i);
		}
		if (jugador1.Rebota(ListDisp2.disp[i])){
			jugador1.reducirTam();
			ListDisp2.EliminarDisp(i);
		}
	}
	
	////Modificacion de trayectoria bolas
	for (int i=0; i<bola.size(); i++){
	if(fondo_izq.Rebota(bola[i]))
	{
		bola[i].centro.x=0;
		bola[i].centro.y=rand()/(float)RAND_MAX;
		bola[i].velocidad.x=2+2*rand()/(float)RAND_MAX;
		bola[i].velocidad.y=2+2*rand()/(float)RAND_MAX;
		bola[i].reducirTam();
		duplicarEsferas();
		puntos2++;
	}

	if(fondo_dcho.Rebota(bola[i]))
	{
		bola[i].centro.x=0;
		bola[i].centro.y=rand()/(float)RAND_MAX;
		bola[i].velocidad.x=-2-2*rand()/(float)RAND_MAX;
		bola[i].velocidad.y=-2-2*rand()/(float)RAND_MAX;
		bola[i].reducirTam();
		duplicarEsferas();
		puntos1++;
			}
		}
	}
	
	//////////////////////
	char cad[500];
	int n;
	n=conexiones.size();
	sprintf(cad,"%f %f %f %f %f %f %f %f %f %f %f %d %d %s %s", 
		bola[0].centro.x,
		bola[0].centro.y, 
		bola[0].radio,
		jugador1.x1,
		jugador1.y1,
		jugador1.x2,
		jugador1.y2, 
		jugador2.x1,
		jugador2.y1,
		jugador2.x2,
		jugador2.y2, 
		puntos1, 
		puntos2,
		nombres[0],
		nombres[1]
		);
		
	for (int i=conexiones.size()-1; i>=0; i--) {
         if (conexiones[i].Send(cad,200) <= 0) {
            printf("Se ha desconectado %s\n",nombres[i]);
            conexiones.erase(conexiones.begin()+i);//borramos el socket para que sea suplantado por el siguiente en cola
            switch (i){
            	case 0:{
            		strcpy(nombres[0],nombres[1]);
            		strcpy(nombres[1],nombres[2]);
            		strcpy(nombres[2],nombres[3]);
            		}break;
            	case 1:{
            		strcpy(nombres[1],nombres[2]);
            		strcpy(nombres[2],nombres[3]);
            		}break;
            	case 2:{
            		strcpy(nombres[2],nombres[3]);
            		}break;
            	case 3:{
            		char nn[50]=" ";
            		strcpy(nombres[3],nn);
            		}break;
            	}
            	num--;//reducimos numero de clientes
            if (i < 2) {// Hay menos de dos clientes conectados
            	bola[0].centro.x=0;
		bola[0].centro.y=0; 
		bola[0].radio=0.5f;
		jugador1.x1=-6;
		jugador1.y1=-1;
		jugador1.x2=-6;
		jugador1.y2=1;
		jugador1.velocidad.x=0; 
            	jugador1.velocidad.y=0; 
		jugador2.x1=6;
		jugador2.y1=-1;
		jugador2.x2=6;
		jugador2.y2=1;
		jugador2.velocidad.x=0; 
            	jugador2.velocidad.y=0;
		puntos1=0; 
		puntos2=0;
            } // Se resetean los puntos a cero
             
         }
     }
}

void CMundo::Init()
{
/////Nombre

for (int i=0;i<4;i++){
	char ini[50]=" ";
	strcpy(nombres[i],ini);
}

//////Comunicacion con Sockets

	int init;
	char ip[]="127.0.0.1";
	init = servidor.InitServer(ip,8000); //iniciamos el socket con IP 127.0.0.1 y al puerto 8000
	if (!init) printf("Server: Created Socket %d\n",1);
	else printf("Server Error: Creating Socket%d\n",1);
	//////////////////
	char nombre[50];

//////////////////Descriptores de ficheros

fd = open(fich, O_WRONLY); //open de la tuberia
if(fd<0) printf("Server Error: Opening Logger Pipe\n");

//////////////creamos el hilo para envio de datos de las teclas

pthread_attr_init(&atribJ1);
pthread_attr_setdetachstate(&atribJ1,PTHREAD_CREATE_JOINABLE);
pthread_create(&thidJ1, NULL, hilo_comandosJ1, this);

pthread_attr_init(&atribJ2);
pthread_attr_setdetachstate(&atribJ1,PTHREAD_CREATE_JOINABLE);
pthread_create(&thidJ2, NULL, hilo_comandosJ2, this);

//////////////creamos el hilo para Conexion de jugadores

pthread_attr_init(&atrib2);
pthread_attr_setdetachstate(&atrib2,PTHREAD_CREATE_JOINABLE);
pthread_create(&thid2, NULL, hilo_comandos2, this);



////////////////////esfera inicial

	Esfera e;
	bola.push_back(e);
	
/////////////////Disparo inicio
	for (int i=0;i<3;i++){
		ListDisp1.Dispara(0,jugador1.Centro());
		ListDisp2.Dispara(0,jugador1.Centro());
	}	
////////////////
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}

void CMundo::duplicarEsferas(){//numero maximo de bolas
	if (bola.size()<1){
		Esfera e;
		e.setRadio(bola[0].radio);
		bola.push_back(e);
		}
	else return;
}
