// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <iostream>

//servicios
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{	
	Init();
}

CMundo::~CMundo()
{
	bola.clear();//borro bolas creadas
	munmap(pMemCompartida,sizeof(MemCompartida));//desenlazo el mapeo de memoria
	com.Close();
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	////////////
	///////////////
	char cad[100];
	sprintf(cad,"%s: %d",nombres[0],puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"%s: %d",nombres[1],puntos2);
	print(cad,450,0,1,1,1);
	
	if (puntos1==4 || puntos2==4){
		char cad[100];
		sprintf(cad,"FIN DEL JUEGO");
		print(cad,220,200,1,1,1);
	}
	
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	ListDisp1.DibujaDisp();
	ListDisp2.DibujaDisp();
	for (int i=0; i< bola.size();i++){
		bola[i].Dibuja();
	}

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)

{	
	
	///////////////Lectura de tuberia///////////////
	char cad[500];
	com.Receive(cad,sizeof(cad));
	sscanf(cad,"%f %f %f %f %f %f %f %f %f %f %f %d %d %s %s", 
			&bola[0].centro.x,
			&bola[0].centro.y, 
			&bola[0].radio,
			&jugador1.x1,
			&jugador1.y1,
			&jugador1.x2,
			&jugador1.y2, 
			&jugador2.x1,
			&jugador2.y1,
			&jugador2.x2,
			&jugador2.y2, 
			&puntos1, 
			&puntos2,
			nombres[0],
			nombres[1]
		);
	////////////////////////////////
	// BOT 
	jugador1.centro=jugador1.Centro();
	pMemCompartida->esfera=bola[0];
	pMemCompartida->raqueta1=jugador1;
	
	switch (pMemCompartida->accion){
		
		case 1:{
			OnKeyboardDown('s', 0, 0);
		}break;
			
		case -1:{
			OnKeyboardDown('w', 0, 0);
		}
		break;
		default:{
			jugador2.velocidad.y=0;
		}break;
	}
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{	
	char cad[]="0";
	switch(key){
	case 'w': {
		sprintf(cad,"w");
		}break;
	case 's': {
		sprintf(cad,"s");
		}break;
	case 'o': {
		sprintf(cad,"o");
		}break;
	case 'l': {
		sprintf(cad,"l");
		}break;
	case 'k': {
		sprintf(cad,"k");
		}break;
	case 'a': {
		sprintf(cad,"a");
		}break;
	}
	com.Send(cad,sizeof(cad));//Enviamos valor recibido del teclado para que funcione en en Servidor
}

void CMundo::Init()
{

/////////////comunicacion con Socket
	printf("Nombre de jugador: ");
	scanf("%s",cliente);	
	if(com.Connect(ip,8000)==0) printf("Client: Connected Socket\n");
	com.Send(cliente, sizeof(cliente));
////////////////////
	Esfera e;
	bola.push_back(e);

/////////proyeccion en memoria///////////
	
	
	MemCompartida.esfera = bola[0];
	MemCompartida.raqueta1=jugador1;
	MemCompartida.accion=0;
	
	fdp=open(fichproy,O_CREAT|O_RDWR|O_TRUNC,0666);
	if(write(fdp,&MemCompartida,sizeof(MemCompartida))){
		pMemCompartida=static_cast<DatosMemCompartida*>(mmap(NULL, sizeof(MemCompartida), PROT_WRITE, MAP_SHARED, fdp,0));
		
		if (pMemCompartida==MAP_FAILED) {
			perror("Error en proyeccion de datos");
			close(fdp);
			return;
		}
	}
	else {
		perror("Error en escritura de fichero de proyeccion");
		close(fdp);
		return;
		}
		
		close(fdp);

////////////////
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}

void CMundo::duplicarEsferas(){//numero maximo de bolas
	if (bola.size()<1){
		Esfera e;
		e.setRadio(bola[0].radio);
		bola.push_back(e);
		}
	else return;
}

void CMundo::inicializarDisp(ListaDisparos list,int d){
	for (int i=0;i<d;i++){
	list.Dispara(0,jugador1.Centro());
	}
}
