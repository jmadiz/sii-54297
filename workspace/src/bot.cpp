#include <DatosMemCompartida.h>


#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>


int main(){

	int fd; 
	char fichproy[100]="/tmp/ProyeccionMemoria";
	int con=0;
	
	DatosMemCompartida* pMemCompartida; //hacemos lo mismo que en Mundo.Init
	
	fd=open(fichproy,O_RDWR);
	if(fd<0){
		perror("error en apertura Fichero proyeccion en Bot.cpp");
		return 1;
	}
	
	pMemCompartida = static_cast<DatosMemCompartida*>(mmap(NULL, sizeof(DatosMemCompartida), PROT_WRITE, MAP_SHARED, fd,0));
		
	if (pMemCompartida==MAP_FAILED) {
		perror("Error en proyeccion de datos");
		close(fd);
		return 1;
	}
	close(fd);
	
	//bola=pMemCompartida->esfera.
	
	////////ya tenemos la posibilidad de modifichar el atributo proyectado
	while(pMemCompartida){
		if ((pMemCompartida->esfera.centro.y) == (pMemCompartida->raqueta1.centro)) {
			pMemCompartida->accion= 0;
			}
		else if((pMemCompartida->esfera.centro.y) > (pMemCompartida->raqueta1.centro)){
			pMemCompartida->accion = -1;
			}
		else if(pMemCompartida->esfera.centro.y < pMemCompartida->raqueta1.centro){
			pMemCompartida->accion = 1;
			}
		usleep(25000);
	}
	
	munmap(pMemCompartida,sizeof(pMemCompartida));
}
