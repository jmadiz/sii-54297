## PRACTICA4 26.12.20

- Se incluye comunicacion mediante Sockets entre el cliente y el Servidor. (v5.0)

- Permanecen las mismas funciones de las versiones anteriores. (v5.0)

- Incluimos la posibilidad de agregar varios clientes en un mismo servidor. Presenta un maximo de 4 clientes entre los cuales 2 seran expectadores y los otros jugadores. (v5.1)

- Cada cliente crea una ventana con su nombre y ocupa el jugador que haya salido anteriormente. (v5.1)

- Toda la informacion de la conexion de nuevos clientes la vemos en el Servidor.(v5.1)


## PRACTICA4 03.12.20

- Se divide el juego entre un Cliente y un Sevidor. Este ultimo se encargara de los calculo de posicion enviaos para que l Cliente pueda dibujarse sin necesidad de calcular las posiciones. 03.12.20

- Se separaron los disparos de la clase Raqueta para que la nueva clase Disparos se encargue de almacenar, dibujar y destruir los disparos segun sea necesario. 06.12.20.

- Incluimos una FIFO en el Cliente para poder comunicar ambos procesos. Ahora podemos eliminar la parte de calculos y movimientos que realiza la funcion OnTimer .  15.12.20 . v4.0.

- Incluimos una nueva FIFO que comunique al Cliente con el Servidor. Asi hemos logramo eliminar las funciones de KeyBoard en el MundoServidor ya que de esto se encargara el Cliente. Solo falta incluir implesion de disparos y varias esferas en el cliente (se proponen incluir para futuras versiones.) 16.12.20. v4.1.

## PRACTICA3 30.11.20

- Se hace la inclusion de un logger que muestre por el terminal std el numero de puntos que tiene acumulado cada jugador. 

- Incluimos un bot que al ser activado se encargue de controlar el jugador 1 (v3.1). Por ahora esta version solo puede jugar con una bola, sin embargo se ampliara el funcionamiento para que permita jugar con multiplicacion de las mismas. 

## PRACTICA2 01.11.20

- Se incluyeron los disparos para cada jugador con un maximo de 3 disparos. Las bolas de destruyen ante el impacto. Principales cambios en la clase Mundo y Raqueta. Tambien la clase Esfera fue cambiada para cumplir con esta funcion. (v2.2)

- Se separa la creacion de los disparos de la clase Mundo. Ahora cada raqueta gestiona la creacion de los mismos de manera dinamica

- Se incluyen mas funciones a la esfera o bola de juego. Esta disminuye sus dimensiones cada vez que impacta con alguna pared de puntuacion y a su vez se dublica. Para ello se incluyo en la clase mundo una lista de esferas que mantuvieran las mismas funciones que la anterior versi'on v2.2. Se propone generar nuevas esferas con distintos colores para las siguientes versiones. (v2.3)

## PRACTICA1 1 15.10.20 

- Se realizo la inclusion de autor del proyecto y a su vez hemos incluido una etiqueta v1.1 que describe esta primera version del mismo.(v1.0,v1.1)

- Hemos incluido el movimiento de la bola de juego (v1.2)

- Se incluye funcionalidad a las raquetan para moverse 





